import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'admin',  loadChildren:()=>import('./admin/admin.module').then(mod=>mod.AdminModule)},
  { path: 'news', loadChildren:()=>import('./news/news.module').then(mod => mod.NewsModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
