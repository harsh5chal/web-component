const fs = require('fs-extra');
const concat = require('concat');
    
build = async () =>{
    const files = [
        'dist/angular-web-component/runtime.js',
        'dist/angular-web-component/polyfills.js',
        'dist/angular-web-component/main.js',
        'dist/angular-web-component/202.js',
        'dist/angular-web-component/533.js'
      ];
    
      await fs.ensureDir('widget-app');
      await concat(files, 'widget-app/news-widget.js');
}
build();